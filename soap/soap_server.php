<?php

	include('helpers\security.php');
	include('includes\shared\ez_sql_core.php');
	include('includes\mysqli\ez_sql_mysqli.php');

	class server{

		private $con;

		// Connecting 
		private function connect(){
			// For security reasons
			$info = new security();
			// Connect to database
			$db = new ezSQL_mysqli(constant('USER'), constant('PASSWORD'), constant('DB'), constant('HOST'));
			if(!$db){
				die('Could not connect to database');
			}
		}

		// Adding a user to database
		function addUser($first_name, $last_name, $username, $password){
			// add user to database
			$insert = $db->query("INSERT INTO `user`(`first_name`, `last_name`, `username`, `password`) VALUES ('$first_name', '$last_name', '$username', '$password')");
			// For debuging
			if(!$insert){
				echo "Error while registering";
			}
			else{
				echo "Register Successful";
			}
		}
	}

	// Setting up Soap Server
	$options = array('uri' => 'http://localhost/');
	$server = new SoapServer(null, $options)
	$server->setClass("server");
    $server->handle();
?>