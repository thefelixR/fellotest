<?php
	include ('soap_auth.php');
	include ('..\helpers\security.php');

	class client{
		function __construct(){
			// Setting up client
			$options = array('location' => "http://fellotest/soap/soap_server.php", 
						 'uri' => 'http://localhost/',
						 'trace'=>1,
						 'connection_timeout' => 300);
			$this->instance = new SoapClient(null, $options);
		}
		// Add user
		public function addUser($first_name, $last_name, $username, $password){
			$this->instance->addUser($first_name, $last_name, $username, $password);
		}
	}

	$client = new client;
?>