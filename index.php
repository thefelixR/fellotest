<!DOCTYPE html>
<html>
	<head>
		<title>Web Test</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>
	<body>
		<div class="row">
		    <div class="col-md-2 col-md-offset-5">
		    	<h1>Fello Webtest</h1><br>
		    	<form name="inputs" action="soap/service.php" method="post">
					<div class="form-group">
						<label for="first_name">First Name</label>
						<input type="text" class="form-control" name="first_name">
					</div>
					<div class="form-group">
						<label for="last_name">Last Name</label>
						<input type="text" class="form-control" name="last_name">
					</div>
					<div class="form-group">
						<label for="email">Username</label>
						<input type="text" class="form-control" name="username">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" name="password">
					</div>
					<div class="form-group">
						<label for="password_confirm">Confirm Password</label>
						<input type="password" class="form-control" name="password_confirm">
					</div>
					<input type="submit" class="btn btn-primary" value="Register">
				</form>
		    </div>
		</div>
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</body>
</html>